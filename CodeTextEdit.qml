import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import SnowModule
import Qt.labs.platform as Platform

Item {
    id:root
    implicitWidth: 200
    implicitHeight: 200
    property alias codeTextColumnNumber: ce.cursorColumnPosition
    property alias codeTextLineNumber: ce.cursorLinePosition
    property alias codeText: codeTextEdit.text
    property alias codeTextEdit: codeTextEdit
    property bool isSaved: true
    property int selectStart
    property int selectEnd
    property int curPos
    property var fw

    focus:true
    Component.onCompleted: {
        ce.init(codeTextEdit.textDocument)
        ce.setLanguage()
        ce.setTheme()
        ce.setFw(fw)
        codeTextEdit.text=ce.fw.readText()
        isSaved=true
    }

    SystemPalette { id: palette; colorGroup: SystemPalette.Active }

    CodeEditor {
        id: ce
        internalCursorPosition: codeTextEdit.cursorPosition
    }
    Rectangle {
        id: background
        color: palette.shadow
        anchors.fill: parent

        Flickable {
            id: flick
            maximumFlickVelocity: 1100
            boundsBehavior: Flickable.StopAtBounds
            anchors.fill: parent

            property int marginsTop: 0
            property int marginsLeft: 0
            property int lineCountWidth: 40
            contentWidth: codeTextEdit.paintedWidth
            contentHeight: codeTextEdit.paintedHeight
            clip: true
            ScrollBar.vertical: ScrollBar {
                id: vbar
                policy: ScrollBar.AlwaysOn
                width: (Qt.platform.os==="android") ? 25 : 15
            }
            ScrollBar.horizontal: ScrollBar {
                id: hbar
                policy: ScrollBar.AlwaysOn
            }

            function ensureVisible(r)
            {
                if (contentX >= r.x)
                    contentX = r.x;
                else if (contentX+width <= r.x+r.width)
                    contentX = r.x+r.width-width;
                if (contentY >= r.y)
                    contentY = r.y;
                else if (contentY+height <= r.y+r.height)
                    contentY = r.y+r.height-height;
            }

            Column {
                            id: lineNumbers
                            anchors.left: parent.left
                            anchors.leftMargin: flick.marginsLeft
                            anchors.topMargin:   flick.marginsTop
                            y:  flick.marginsTop
                            width: flick.lineCountWidth

                            function range(start, end) {
                                var rangeArray = new Array(end-start);
                                for(var i = 0; i < rangeArray.length; i++){
                                    rangeArray[i] = start+i;
                                }
                                return rangeArray;
                            }

                            Repeater {
                                model: codeTextEdit.lineCount
                                delegate:
                                    Label {
                                        id:delegateLabel
                                        //color: Material.primaryTextColor
                                        font: codeTextEdit.font
                                        width: parent.width
                                        height: ce.height(index)
                                        horizontalAlignment: Text.AlignRight
                                        verticalAlignment: Text.AlignVCenter
                                        renderType: Text.NativeRendering
                                        text: index+1
                                        color:palette.highlightedText
                                        background: Rectangle {
                                            color: palette.shadow
                                            border.color: palette.shadow
                                        }
                                }
                            }
                        }
                        Rectangle {
                            y: 0
                            height: parent.height
                            anchors.left: parent.left
                            anchors.leftMargin: flick.lineCountWidth + flick.marginsLeft
                            width: 1
                            color: "green"
                        }
            TextEdit {
                id: codeTextEdit
                //width: flick.width
                //height: flick.height
                leftPadding: flick.marginsLeft+flick.lineCountWidth+4
                rightPadding: flick.marginsLeft
                topPadding: flick.marginsTop
                bottomPadding: flick.marginsTop
                textFormat: TextEdit.PlainText
                overwriteMode: false
                selectByMouse: true
                persistentSelection: true
                cursorVisible: true
                wrapMode: Text.NoWrap
                font.pixelSize: 16
                focus: true
                renderType: Text.NativeRendering
                onCursorRectangleChanged: {
                    flick.ensureVisible(cursorRectangle)
                }
                color: palette.highlightedText
                onTextChanged: {
                    isSaved=false
                }

                MouseArea {
                    id:ms
                    anchors.fill: codeTextEdit
                    propagateComposedEvents: true
                    acceptedButtons: Qt.RightButton
                    hoverEnabled: true
                    onClicked: {
                        selectStart = codeTextEdit.selectionStart;
                        selectEnd = codeTextEdit.selectionEnd;
                        curPos = codeTextEdit.cursorPosition;
                        contextMenu.x = mouse.x;
                        contextMenu.y = mouse.y;
                        contextMenu.open();
                        codeTextEdit.cursorPosition = curPos;
                        codeTextEdit.select(selectStart,selectEnd);
                    }
                    onPressAndHold: {
                        if (mouse.source === Qt.MouseEventNotSynthesized) {
                            selectStart = codeTextEdit.selectionStart;
                            selectEnd = codeTextEdit.selectionEnd;
                            curPos = codeTextEdit.cursorPosition;
                            contextMenu.x = mouse.x;
                            contextMenu.y = mouse.y;
                            contextMenu.open()
                            codeTextEdit.cursorPosition = curPos;
                            codeTextEdit.select(selectStart,selectEnd);
                        }
                    }
                }
                Menu {
                    id: contextMenu
                    MenuItem {
                        text: "Cut"
                        onTriggered: {
                            codeTextEdit.cut()
                        }
                    }
                    MenuItem {
                        text: "Copy"
                        onTriggered: {
                            codeTextEdit.copy()
                        }
                    }
                    MenuItem {
                        text: "Paste"
                        onTriggered: {
                            codeTextEdit.paste()
                        }
                    }
                }

            }
        }
    }
    function saveFile(){
        var res = ce.fw.saveText(codeTextEdit.text)
        isSaved=true
        console.log("Saved file from editor")
    }
    Platform.MessageDialog {
        id: messageDialogFileChangedOnDisk
        property var cb
        title: "File changed"
        text: "File changed on disk, reload file?"
        buttons: Platform.MessageDialog.Ok | Platform.MessageDialog.Cancel
        onAccepted: {
            codeText = ce.fw.readText()
        }
    }
    Connections {
        target: ce.fw
        function onFileChangedOnDisk(path) { console.log("File changed signal "+path) }
    }

//    function fileChangedOnDisk(path){
//        console.log("Test, got signal file changed on disk "+path)
//        var tmp = ce.fw.url.toString().replace("file://", "")
//        if(tmp===path){
//            if(ce.fw.readText()!==codeText){
//                //this should go through all filewrappers and reload the correct one
//                //messageDialogFileChangedOnDisk.open()
//            }
//        }
//    }
}
