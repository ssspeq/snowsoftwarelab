
#include "codeeditor.h"

CodeEditor::CodeEditor(QObject *parent) : QObject(parent)
{

}

void CodeEditor::init(QQuickTextDocument *document)
{
    m_doc = document;

    QTextOption textOptions = m_doc->textDocument()->defaultTextOption();
    //textOptions.setTabStopDistance()
    textOptions.setTabStopDistance(16);
    m_doc->textDocument()->setDefaultTextOption(textOptions);
    m_hightliter = new QSourceHighlite::QSourceHighliter(m_doc->textDocument());
    //connect(m_doc->textDocument(), &QTextDocument::blockCountChanged, this, &CodeEditorBackend::lineCountChanged);
    //We can get this from qml
}

void CodeEditor::setLanguage()
{
    m_hightliter ->setCurrentLanguage(QSourceHighlite::QSourceHighliter::CodeQML);
}

void CodeEditor::setTheme()
{
    m_hightliter->setTheme(QSourceHighlite::QSourceHighliter::Themes::Monokai);
}

int CodeEditor::height(int lineNumber)
{
    if (this->doc()==nullptr)
        return 0;
    return int(m_doc->textDocument()->documentLayout()->blockBoundingRect(m_doc->textDocument()->findBlockByNumber(lineNumber)).height());
}
