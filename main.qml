import QtQuick.Controls
import Qt.labs.settings
import QtQuick.Layouts
import QtQuick
import Qt.labs.platform as Platform
import SnowModule
import Qt.labs.folderlistmodel
import "./ListProperty.js" as Ls

ApplicationWindow {
    width: 1024
    height: 768
    visible: true
    id: window
    title: qsTr("Snow Software Lab")

    Settings {
        id: settings
        property alias x: window.x
        property alias y: window.y
        property alias width: window.width
        property alias height: window.height
        property string wmMode: "1"
        property int wmColumns
        property int wmRows
    }

    Wm {
        id: wm
        dropArea:mainArea
    }

    SnowIDE {
        id: ide
    }

    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    header:
        ToolBar {
        id: toolBar
        contentHeight: toolButtonForDrawerLeft.implicitHeight

        ToolButton {
            id: toolButtonForDrawerLeft
            text: "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                drawerLeft.open()
            }
        }
        ToolButton {
            id: buttonWindowControl
            action: resizeWindowsAction
            anchors.left: toolButtonForDrawerLeft.right
            onPressAndHold: {
                //Show overview when implemented
            }
        }
        ToolButton {
            id: buttonSettings
            action: windowSettingsAction
            anchors.left: buttonWindowControl.right
            onClicked: {
                windowSettings.open()
            }
        }

        ToolButton {
            id: toolButtonForDrawerRight
            text: "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            anchors.right: parent.right
            onClicked: {
                drawerRight.open()
            }
        }

        Menu {
            id: projectMenu
            MenuItem {
                id: menuItemOpenProject
                action: openProjectAction
                text: qsTr("Open Project")
            }
            MenuItem {
                id: menuItemNewProject
                action: newProjectAction
                text: qsTr("New Project")
            }
            MenuItem {
                id: menuItemCloseProject
                action: closeProjectAction
                enabled: !ide.currentProject ? false : true
                text: qsTr("Close Current Project")
            }
            MenuItem {
                id: newFile
                action: newFileAction
                enabled: !ide.currentProject ? false : true
                text: qsTr("New File")
            }
        }
        Menu {
            id: selectCurrentProjectMenu
            Repeater {
                model: ide.projects
                MenuItem {
                    text: dataObject.name
                    onClicked: {
                        ide.currentProject=dataObject
                    }
                }
            }
        }
    }

    Drawer {
        id: drawerLeft
        width: 250
        height: window.height
        ToolBar {
            id: leftDrawerToolBar
            width: drawerLeft.width
            height: 50
        }
        ToolButton {
            id: projectMenuToolButton
            text: qsTr("⋮")
            anchors.left: leftDrawerToolBar.left
            onClicked: {
                projectMenu.open()
            }
        }
        ToolButton {
            anchors.left: projectMenuToolButton.right
            action: runProjectAction
        }
        ToolButton {
            text: ide.currentProject ? ide.currentProject.name : ""
            visible: ide.projects.count===0 ? false : true
            anchors.right: leftDrawerToolBar.right
            onClicked:selectCurrentProjectMenu.open()
        }
        ColumnLayout{
            spacing: 0
            width: parent.width
            height: drawerLeft.height-leftDrawerToolBar.height
            anchors.top: leftDrawerToolBar.bottom
            StackLayout {
                Layout.alignment: Qt.AlignTop
                Layout.preferredWidth: drawerLeft.width
                Layout.preferredHeight: drawerLeft.height-leftDrawerToolBar.height
                currentIndex: bar.currentIndex
                Item {
                    id: filesTab
                    ProjectFilesView {
                        id: filesList
                        anchors.fill: parent
                        visible: !ide.currentProject ? false : true
                        model: !ide.currentProject ? false : ide.currentProject.files
                    }
                }
                Item {
                    id: variablesTab
                }
                Item {
                    id: databaseTab
                }
            }
            TabBar {
                id: bar
                Layout.alignment: Qt.AlignBottom
                Layout.fillHeight: true
                Layout.preferredWidth: drawerLeft.width
                Layout.preferredHeight: leftDrawerToolBar.height
                TabButton {
                    text: qsTr("Files")
                }
                TabButton {
                    text: qsTr("Variables")
                }
                TabButton {
                    text: qsTr("Database")
                }
            }
        }
    }

    Drawer {
        id: drawerRight
        width: 250
        height: window.height
        edge : Qt.RightEdge
        ToolBar {
            id: leftRightToolBar
            width: drawerLeft.width
            height: 50
        }
    }
    RecentProjectsView {
        id: recentProjectsView
        anchors.centerIn: parent
        height: 400
        width: 300
    }
    DropArea {
        id: mainArea
        width: window.width;
        height: window.height-toolBar.height
        anchors.top: toolBar.bottom
        Rectangle {
            anchors.fill: parent
            color: "green"
            visible: parent.containsDrag
        }
    }

    Action {
        id: openProjectAction
        text: qsTr("Open project")
        icon.name: "document-open"
        shortcut: "Ctrl+O"
        onTriggered: {
            drawerLeft.close()
            fileDialog.open()
        }
    }
    Action {
        id: newProjectAction
        text: qsTr("New project")
        icon.name: "document-new"
        shortcut: "Ctrl+N"
        onTriggered: {
            drawerLeft.close()
            newProjectDialog.open()
        }
    }
    Action {
        id: closeProjectAction
        text: qsTr("Close current project")
        icon.name: "gtk-close"
        //shortcut: "Ctrl+N"
        onTriggered: {
            ide.closeProject(ide.currentProject)
            ide.setCurrentProjectToFirstOpenProject()
        }
    }
    Action {
        id: runProjectAction
        icon.name: "run-build"
        enabled: !ide.currentProject ? false : true
        //shortcut: "Ctrl+N"
        onTriggered: {
            drawerLeft.close()
            wm.runProject(ide.currentProject.projectFolder+"/main.qml")
        }
    }
    Action {
        id: resizeWindowsAction
        icon.name: "view-grid"
        enabled: !ide.currentProject ? false : true
        //shortcut: "Ctrl+N"
        onTriggered: {
            wm.resizeWindows(settings.wmRows, settings.wmColumns)
        }
    }
    Action {
        id: windowSettingsAction
        icon.name: "settings-configure"
        enabled: !ide.currentProject ? false : true
        //shortcut: "Ctrl+N"
        onTriggered: {

        }
    }
    Action {
        id: newFileAction
        icon.name: "document-new"
        enabled: !ide.currentProject ? false : true
        //shortcut: "Ctrl+N"
        onTriggered: {
            newFileDialog.open()
        }
    }

    Dialog {
        id: newProjectDialog
        title: "New Project"
        modal: true
        parent: Overlay.overlay
        anchors.centerIn: parent
        width: 400
        height: 600
        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: {
            ide.makeNewProject(dirInput.text, nameInput.text)
            console.log("Made new project")
        }
        onRejected: console.log("Cancel clicked")
        Column {
            anchors.fill: parent
            TextField {
                id: nameInput
                text: ""
                placeholderText: "Name"
            }
            TextField {
                id: dirInput
                text: folderDialog.folder
                placeholderText: "Directory"
            }
            Button{
                text: "Folder"
                onClicked: {
                    folderDialog.open()
                }
            }
        }
    }

    Dialog {
        id: newFileDialog
        title: "New File"
        modal: true
        parent: Overlay.overlay
        anchors.centerIn: parent
        standardButtons: Dialog.Ok | Dialog.Cancel
        onOpened: {
                    standardButton(Dialog.Ok).enabled = false;
                }
        onAccepted: {
            console.log("OK clicked")
            if(qmlBox.checked){addNewFile(newFileNameInput.text+".qml")}
            else if(javaScriptBox.checked){addNewFile(newFileNameInput.text+".js")}
        }
        onRejected: console.log("Cancel clicked")
        contentItem: ColumnLayout {
            implicitWidth: 400
            implicitHeight: 200
            GroupBox {
                title: qsTr("File type")
                ColumnLayout {
                    anchors.fill: parent
                    ButtonGroup {
                            id: fileTypeGroup
                            exclusive: true
                        }
                    CheckBox {
                        text: qsTr("QML")
                        id: qmlBox
                        checked: true
                        ButtonGroup.group: fileTypeGroup
                    }
                    CheckBox {
                        text: qsTr("Javascript")
                        id: javaScriptBox
                        ButtonGroup.group: fileTypeGroup
                    }
                }
            }
            TextField {
                id: newFileNameInput
                text:""
                onTextChanged: {
                    //FIX THIS TO PROPERLY CHECK INPUT
                    if(text!==""){
                        newFileDialog.standardButton(Dialog.Ok).enabled = true;
                    }
                    else{
                        newFileDialog.standardButton(Dialog.Ok).enabled = false;
                    }
                }
            }
        }
    }

    Dialog {
        id: renameFileDialog
        title: "Rename file"
        property string oldFileName
        modal: true
        parent: Overlay.overlay
        anchors.centerIn: parent
        standardButtons: Dialog.Ok | Dialog.Cancel
        onOpened: newNameInput.text=renameFileDialog.oldFileName
        onAccepted: {
            console.log("OK clicked")
            console.log(renameFileDialog.oldFileName+" "+newNameInput.text)
            renameFile(renameFileDialog.oldFileName, newNameInput.text)
        }
        onRejected: console.log("Cancel clicked")
        contentItem: ColumnLayout {
            implicitWidth: 400
            implicitHeight: 200
            Label {
                text:"New name for "+renameFileDialog.oldFileName
            }
            TextField {
                id: newNameInput
                onTextChanged: {
                    //FIX THIS TO PROPERLY CHECK INPUT
                    if(text!==renameFileDialog.oldFileName){
                        newFileDialog.standardButton(Dialog.Ok).enabled = true;
                    }
                    else{
                        newFileDialog.standardButton(Dialog.Ok).enabled = false;
                    }
                }
            }
        }
    }


    Dialog {
        id: removeFileDialog
        title: "Remove file"
        property string file
        modal: true
        parent: Overlay.overlay
        anchors.centerIn: parent
        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: {
            console.log("OK clicked")
            removeFile(file, removeOnDiskCheckbox.checked)
        }
        onRejected: console.log("Cancel clicked")
        contentItem: ColumnLayout {
            implicitWidth: 400
            implicitHeight: 200
            Label {
                text:"Do you really want to remove "+removeFileDialog.file+" from project?"
            }
            CheckBox {
                id: removeOnDiskCheckbox
                text: "Delete file on disk"
                checked: true
            }
        }
    }


    Dialog {
        id: quitDialog
        modal: true
        anchors.centerIn: parent
        title: "Quit application?"
        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: {
            console.log("Quit dialog Ok clicked")
            quitDialog.close()
            Qt.quit()
        }
        onRejected: {
            console.log("Quit dialog cancel clicked")
            quitDialog.close()
        }
    }

    ErrorMessage {
        id: errorMessage
        enabled: false
        anchors.centerIn: mainArea
    }

    Platform.FolderDialog {
        id: folderDialog
        title: "Please choose a folder"
        folder: Platform.StandardPaths.writableLocation(Platform.StandardPaths.DocumentsLocation)
        options: {
            Platform.FolderDialog.ShowDirsOnly
        }
    }

    Platform.FileDialog {
        id: fileDialog
        currentFile: file
        folder: Platform.StandardPaths.writableLocation(Platform.StandardPaths.DocumentsLocation)
        nameFilters: ["Snow Software Lab Project File (*.proj)"]
        onAccepted: {
            ide.loadProject(Qt.resolvedUrl(file))
        }
        onRejected: {
            console.log("Canceled")
        }
    }

    Menu {
        id: windowSettings
        MenuItem { text: "Preferences"
            //action: openProjectAction
        }

        MenuSeparator { }
        Menu {
            title: "WM settings"
            Label{
                text:"Columns"
            }
            SpinBox {
                id:columnsSpinBox

                value:1
                onValueChanged: settings.wmColumns=value
            }
            Label{
                text:"Rows"
            }
            SpinBox {
                id:rowsSpinBox

                value:1
                onValueChanged: settings.wmRows=value
            }
            Action {checkable: true; text: qsTr("Free windows"); ActionGroup.group: wmActions }
        }

        ActionGroup {
            id: wmActions
            onTriggered:
            {
                settings.wmMode = action.text
            }
        }
        Component.onCompleted: {
            //Need to set active action here from settings
            for (var i = 0; i < wmActions.actions.length; i++)  {
                if(settings.wmMode === wmActions.actions[i].text){
                    wmActions.checkedAction = wmActions.actions[i]
                    break
                }
            }
            columnsSpinBox.value = settings.wmColumns
            rowsSpinBox.value = settings.wmRows
        }
    }


    onClosing: {
        if (Qt.platform.os === "android" || Qt.platform.os === "ios") {
            console.log("Back button pressed")
            close.accepted = false;
            quitDialog.open();
        }
    }

    function openFile(file){
        //Change these functions to accept project
        var fw = ide.currentProject.getFileWrapper(file)
        console.log(fw)
        wm.editFile(fw)
        drawerLeft.close()
    }

    function addNewFile(file){
        ide.addNewFileToProject(ide.currentProject, file)
    }

    function renameFile(file, newFile){
        ide.renameFile(ide.currentProject, file, newFile)
    }

    function removeFile(file, deleteOnDisk){
        ide.removeFileFromProject(ide.currentProject, file, deleteOnDisk)
    }

    Connections {
        target: ide
        function onProjectLoaded() { }
    }
    Connections {
        target: ide
        function onProjectClosed() { }
    }
    Connections {
        target: ide
        function onError(error) { errorMessage.show(error) }
    }

    Connections {
        target: filesList
        function onOpenFile(file) {
            openFile(file)
        }
    }
    Connections {
        target: filesList
        function onRemoveFile(file) {
            removeFileDialog.file=file
            removeFileDialog.open()
        }
    }
    Connections {
        target: filesList
        function onRenameFile(file) {
            console.log("Rename file "+file)
            renameFileDialog.oldFileName=file
            renameFileDialog.open()
        }
    }
}
