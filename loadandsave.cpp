#include "loadandsave.h"

LoadAndSave::LoadAndSave(QObject *parent) : QObject(parent)
{

}

QByteArray LoadAndSave::serializeProject(QObject *obj)
{
    vs::QtSerialize serialize;
    serialize.addBlackListItem("openFiles");
    QByteArray result = serialize.serialize(obj);
    return result;
}

QObject* LoadAndSave::deSerializeProject(QUrl url)
{
    vs::QtDeSerialize reader;
    QByteArray ba = readFromFile(url);
    Project *project = new Project();
    //std::unique_ptr<MyFactory> factory(new MyFactory);
    //reader.setObjectFactory(std::move(factory));
    bool ok = reader.deserialize(project, ba);
    if(ok){
        return project;
    }
    else{
        return nullptr;
    }
}

int LoadAndSave::writeToFile(QUrl url, QByteArray ba)
{
    //TODO: Add error handling
    QSaveFile file(url.toLocalFile());
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "failed to open";
        return -1;
    }
    file.write(ba);
    // Calling commit() is mandatory, otherwise nothing will be written.
    file.commit();
    return 0;
}

QByteArray LoadAndSave::readFromFile(QUrl url)
{
    //TODO: Add error handling
    QFile f(url.toLocalFile());
        if (!f.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            qDebug() << "failed to open";
            return nullptr;
        }

        QByteArray ba = f.readAll();
        //qDebug() << "read" << ba.size();
        return ba;
}

int LoadAndSave::makeFile(QUrl file)
{
    QFile mainFile(file.toLocalFile());
    qDebug() << "In create file, filename is: "+mainFile.fileName();

    if (!mainFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
            qDebug() << "File error: "+file.errorString();
            return -1;
        }
    else{
        mainFile.close();
        return 0;
    }
}

int LoadAndSave::deleteFile(QUrl file)
{
    QFile mainFile(file.toLocalFile());
    qDebug() << "In remove file, filename is: "+mainFile.fileName();
    if(mainFile.remove()){
        return 0;
    }
    return -1;
}
