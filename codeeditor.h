#ifndef CODEEDITOR_H
#define CODEEDITOR_H

#include <QObject>
#include <QQuickTextDocument>
#include <QAbstractTextDocumentLayout>
#include <QQuickTextDocument>
#include <qsourcehighliter.h>
#include <QtQml>
#include <filewrapper.h>
#include <QTextCursor>
#include <QTextBlock>

class CodeEditor : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QSourceHighlite::QSourceHighliter* hightliter READ hightliter WRITE setHightliter NOTIFY hightliterChanged)
    Q_PROPERTY(QQuickTextDocument* doc READ doc WRITE setDoc NOTIFY docChanged)
    Q_PROPERTY(QTextCursor* cursor READ cursor WRITE setCursor NOTIFY cursorChanged)
    Q_PROPERTY(int internalCursorPosition READ internalCursorPosition WRITE setInternalCursorPosition NOTIFY internalCursorPositionChanged)
    Q_PROPERTY(int cursorLinePosition READ cursorLinePosition WRITE setCursorLinePosition NOTIFY cursorLinePositionChanged)
    Q_PROPERTY(int cursorColumnPosition READ cursorColumnPosition WRITE setCursorColumnPosition NOTIFY cursorColumnPositionChanged)
    Q_PROPERTY(FileWrapper* fw READ fw WRITE setFw NOTIFY fwChanged)
    QML_ELEMENT

    QSourceHighlite::QSourceHighliter* m_hightliter = nullptr;
    QQuickTextDocument* m_doc = nullptr;
    FileWrapper* m_fw = new FileWrapper(this);

    QTextCursor* m_cursor = nullptr;

    int m_internalCursorPosition = 0;

    int m_cursorLinePosition = 1;

    int m_cursorColumnPosition = 0;


public:
    Q_INVOKABLE explicit CodeEditor(QObject *parent = nullptr);

QSourceHighlite::QSourceHighliter* hightliter() const
{
    return m_hightliter;
}

QQuickTextDocument* doc() const
{
    return m_doc;
}

int internalCursorPosition() const
{
    return m_internalCursorPosition;
}

int cursorLinePosition() const
{
    return m_cursorLinePosition;
}

QTextCursor* cursor() const
{
    return m_cursor;
}

int cursorColumnPosition() const
{
    return m_cursorColumnPosition;
}

FileWrapper* fw() const
{
    return m_fw;
}

public slots:
    void init(QQuickTextDocument* document);
    void setLanguage();
    void setTheme();
    int height(int lineNumber);

void setHightliter(QSourceHighlite::QSourceHighliter* hightliter)
{
    if (m_hightliter == hightliter)
        return;

    m_hightliter = hightliter;
    emit hightliterChanged(m_hightliter);
}

void setDoc(QQuickTextDocument* doc)
{
    if (m_doc == doc)
        return;

    m_doc = doc;
    emit docChanged(m_doc);
}

void setInternalCursorPosition(int internalCursorPosition)
{
    if (m_internalCursorPosition == internalCursorPosition)
        return;

    m_internalCursorPosition = internalCursorPosition;
    emit internalCursorPositionChanged(m_internalCursorPosition);
    this->setCursorLinePosition(m_internalCursorPosition);
    this->setCursorColumnPosition(m_internalCursorPosition);
}

void setCursorLinePosition(int cursorPosition)
{
    if (this->doc()==nullptr){
        return;
    }
    QTextCursor cursor(this->doc()->textDocument());
    cursor.setPosition(cursorPosition);
    cursor.movePosition(QTextCursor::StartOfLine);

        int lines = 1;
        while(cursor.positionInBlock()>0) {
            cursor.movePosition(QTextCursor::Up);
            lines++;
        }
        QTextBlock block = cursor.block().previous();

        while(block.isValid()) {
            lines += block.lineCount();
            block = block.previous();

        }
    if (m_cursorLinePosition == lines)
        return;

    m_cursorLinePosition = lines;
    emit cursorLinePositionChanged(m_cursorLinePosition);
}

void setCursor(QTextCursor* cursor)
{
    if (m_cursor == cursor)
        return;

    m_cursor = cursor;
    emit cursorChanged(m_cursor);
}

void setCursorColumnPosition(int cursorPosition)
{
    if (this->doc()==nullptr){
        return;
    }
    QTextCursor cursor(this->doc()->textDocument());
    cursor.setPosition(cursorPosition);
    cursor.movePosition(QTextCursor::StartOfLine);
    int column = cursorPosition-cursor.position();

    if (m_cursorColumnPosition == column)
        return;

    m_cursorColumnPosition = column;
    emit cursorColumnPositionChanged(m_cursorColumnPosition);
}

void setFw(FileWrapper* fw)
{
    if (m_fw == fw)
        return;

    m_fw = fw;
    emit fwChanged(m_fw);
}

signals:

void hightliterChanged(QSourceHighlite::QSourceHighliter* hightliter);
void docChanged(QQuickTextDocument* doc);
void internalCursorPositionChanged(int internalCursorPosition);
void cursorLinePositionChanged(int cursorLinePosition);
void cursorChanged(QTextCursor* cursor);
void cursorColumnPositionChanged(int cursorColumnPosition);
void fwChanged(FileWrapper* fw);
};

#endif // CODEEDITORBACKEND_H

