import QtQuick
import QtQuick.Controls
import SnowModule

Item {
    id:root
    anchors.fill: parent
    property alias fw: fw
    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    FileWrapper {
        id: fw
    }

    Rectangle {
        color: myPalette.dark
        visible: errorMessage.text !== ""
        anchors.centerIn: parent
        width: parent.width
        height: parent.height
        Text {
            id: errorMessage
            anchors { left: parent.left; right: parent.right; top: parent.top; margins: 20; topMargin: 10 }
            font.pointSize: 20
            wrapMode: Text.WordWrap
            text: ""
            color: myPalette.highlightedText
        }
    }
    Loader {
        id: load

        function reload() {
            source = "";
            $QmlEngine.clearCache();
            source = fw.url;
        }
        //asynchronous: true
        anchors.fill: parent
        source: fw.url
        onStatusChanged: {
            if( load.status == Loader.Error ){
                console.debug("Error:"+ load.sourceComponent.errorString());
                errorMessage.text=load.sourceComponent.errorString()
                load.source=""

            }
            else if(load.status == Loader.Ready){
                errorMessage.text=""
            }
        }
    }

    Connections {
        target: fw
        onFileChangedOnDisk: {
            console.log("In userapploader, got filechangedondisk")
            load.reload()
        }
    }
}
