#ifndef WINDOWMANAGER_H
#define WINDOWMANAGER_H

#include <QObject>
#include <qqml.h>

class WindowManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject*> windows READ windows WRITE setWindows NOTIFY windowsChanged)
    QML_ELEMENT
    QList<QObject*> m_windows;

public:
    explicit WindowManager(QObject *parent = nullptr);

    QList<QObject*> windows() const
    {
        return m_windows;
    }

public slots:
    void setWindows(QList<QObject*> windows)
    {
        if (m_windows == windows)
            return;

        m_windows = windows;
        emit windowsChanged(m_windows);
    }

signals:

    void windowsChanged(QList<QObject*> windows);
};

#endif // WINDOWMANAGER_H
