import QtQuick
import QtQuick.Controls
import QtQuick.Layouts


FocusScope {
    id: root
    //For resizing in forced resize state
    property int windowId
    property int lastPositionX
    property int lastPositionY
    property int lastWidth
    property int lastHeight
    property int mNewX
    property int mNewY
    property int mNewWidth
    property int mNewHeight
    //To keep window from moving outside of the visible screen area
    property int minX:0-width/2
    property int maxX:parent.width-width/2
    property int minY:0
    property int maxY:parent.height-title.height
    property int maxWidth:parent.width
    property int maxHeight:parent.height

    signal titlePressed(int id)
    signal windowPressed(int id)
    signal closePressed(int windowId)
    //For window management
    property string titleText: ""
    property string lastState
    property string contents
    //For states
    property bool isManuallyResizing:false
    property bool isForcedResizing:false
    //property FileWrapper fw


    onXChanged: {

    }
    onYChanged: {

    }
    onFocusChanged: {
        console.log("WindowId:"+windowId+" Focus changed, focus is:"+focus)

    }
    onActiveFocusChanged: {
        console.log("WindowId:"+windowId+" Focus changed, activefocus is:"+activeFocus)
    }
    Component.onCompleted: {
        lastPositionX=x
        lastPositionY=y
        lastWidth=width
        lastHeight=height
        console.log(maxX)
        console.log(maxY)
        console.log(maxWidth)
        console.log(maxHeight)
    }

    function animatedResize(x, y, width, height){
        mNewX = x
        mNewY = y
        mNewWidth = width
        mNewHeight = height
        lastPositionX = root.x
        lastPositionY = root.y
        lastWidth = root.width
        lastHeight = root.height
        isForcedResizing=true
    }
    function animatedRestore(){
        mNewX = lastPositionX
        mNewY = lastPositionY
        mNewWidth = lastWidth
        mNewHeight = lastHeight
        isForcedResizing=true
    }
    function animatedMinimize(){
        visible=false
    }

    states: [

        State {
            name: ""
            when:focus&&!isForcedResizing
            PropertyChanges { target: leftBall; visible: false}
            PropertyChanges { target: rightBall; visible: false}
            PropertyChanges { target: topBall; visible: false}
            PropertyChanges { target: bottomBall; visible: false}
            PropertyChanges { target: sizeRect; visible: false}
            PropertyChanges { target: title; color: Material.backgroundColor}
            PropertyChanges { target: root; opacity: 1}
            PropertyChanges { target: root; z: 1; explicit: true;
                restoreEntryValues: false
            }

        },
        State {
            name: "OUTOFFOCUS"
            when:!focus&&!isForcedResizing
            PropertyChanges { target: leftBall; visible: false}
            PropertyChanges { target: rightBall; visible: false}
            PropertyChanges { target: topBall; visible: false}
            PropertyChanges { target: bottomBall; visible: false}
            PropertyChanges { target: sizeRect; visible: false}
            PropertyChanges { target: title; color: Material.dialogColor}
            PropertyChanges { target: root; opacity: 1}
            PropertyChanges { target: root; z: 0; explicit: true;
                restoreEntryValues: false
            }

        },
        State {
            name: "FORCEDRESIZE"
            when:isForcedResizing
            PropertyChanges { target: title; color: Material.backgroundColor}
            PropertyChanges { target: leftBall; visible: false}
            PropertyChanges { target: rightBall; visible: false}
            PropertyChanges { target: topBall; visible: false}
            PropertyChanges { target: bottomBall; visible: false}
            PropertyChanges { target: sizeRect; visible: false}
            PropertyChanges {
                target: root;
                explicit: true;
                restoreEntryValues: false
                x: mNewX;
                y: mNewY;
                width: mNewWidth;
                height: mNewHeight;
            }
        },
        State {
            name:"INVISIBLE";
            when:!visible
            PropertyChanges {   target: root; opacity: 0.0 ;}
        }
    ]
    transitions: [
        Transition {
            from: ""
            to: "FORCEDRESIZE"
            PropertyAnimation { target: root; properties: "x, y, width, height"; easing.type: Easing.InOutQuad }
            onRunningChanged: {
                if ((state === "FORCEDRESIZE") && (!running))
                {
                    console.log("Resetting isForcedResizing")
                    //root.state = root.lastState
                    isForcedResizing=false
                }
            }
        },
        Transition {
            from: "OUTOFFOCUS"
            to: "FORCEDRESIZE"
            PropertyAnimation { target: root; properties: "x, y, width, height"; easing.type: Easing.InOutQuad }
            onRunningChanged: {
                if ((state === "FORCEDRESIZE") && (!running))
                {
                    console.log("Resetting isForcedResizing")
                    //root.state = root.lastState
                    isForcedResizing=false
                }
            }
        },
        Transition {
            from: ""
            to: "INVISIBLE"
            NumberAnimation { property: "opacity"; duration: 500}
        },
        Transition {
            from: "OUTOFFOCUS"
            to: "INVISIBLE"
            NumberAnimation { property: "opacity"; duration: 500}
        },
        Transition {
            from: "INVISIBLE"
            to: ""
            NumberAnimation { property: "opacity"; duration: 500}
        },
        Transition {
            from: "INVISIBLE"
            to: "OUTOFFOCUS"
            NumberAnimation { property: "opacity"; duration: 500}
        }
    ]


    Timer {
        id: timer
        interval: 2000; running: false; repeat: false; triggeredOnStart: false
        onTriggered: {
            //Damnit, now I need the previous state here too
            console.log(root.lastState);
            //root.state = root.lastState
            console.log("timeout");
            isManuallyResizing=false
        }
    }

    /*    RectangularGlow {
           id: effect
           anchors.fill: nodeRectangle
           glowRadius: 40
           spread: 0.1
           color: "black"
           cornerRadius: 25
       }
*/

    Rectangle {
        id: nodeRectangle
        property bool isDragging
        property int offsetX : 0
        property int offsetY : 0
        QtObject {
            id: lastPosition
            property int x : root.x
            property int y : root.y
        }
        anchors.fill: parent
        opacity: 1
        clip: false
        Rectangle {
            id: title
            clip: true
            anchors.top: nodeRectangle.top
            width: nodeRectangle.width
            height: 40
            color: Material.backgroundColor

            MouseArea {
                id: mouseAreaTitle
                anchors.fill: title
                onPressed: {
                    //THIS CHANGES STATE FROM RESIZE TO ""
                    root.forceActiveFocus()
                    console.log("title pressed")
                    wm.windowClicked()
                    nodeRectangle.isDragging = true
                    var globalMouse = root.mapToGlobal(mouse.x, mouse.y)
                    nodeRectangle.offsetX = globalMouse.x - root.x
                    nodeRectangle.offsetY = globalMouse.y - root.y
                }
                onPositionChanged: {
                    if (nodeRectangle.isDragging) {
                        var globalMouse = nodeRectangle.parent.mapToGlobal(mouse.x, mouse.y);
                        root.x = (lastPosition.x - nodeRectangle.parent.x) + globalMouse.x - nodeRectangle.offsetX;
                        root.y = (lastPosition.y - nodeRectangle.parent.y) + globalMouse.y - nodeRectangle.offsetY;
                        root.lastPositionX = root.x
                        root.lastPositionY = root.y
                        root.lastWidth = root.width
                        root.lastHeight = root.height
                        //TODO: fix this
                        //                        if(root.x>maxX){
                        //                            console.log("x exceeded")
                        //                            //nodeRectangle.isDragging=false
                        //                            animatedResize(maxX, y, width, height)
                        //                        }
                        //                        if(root.y>maxY){
                        //                            console.log("y exceeded")
                        //                            //nodeRectangle.isDragging=false
                        //                            animatedResize(x, maxY, width, height)
                        //                        }

                    }
                }
            }
            RowLayout {
                anchors.fill: title
                //Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                spacing:0
                Label {
                    id: titleText
                    color: Material.primaryTextColor
                    text: "test"
                    //elide: Label.ElideRight
                    Layout.alignment:  Qt.AlignHCenter
                    Layout.fillWidth: true
                }
                ToolButton {
                    id: buttonWindowResize
                    icon.name: "edit-paste"
                    icon.source: "images/edit.png"
                    Layout.alignment:  Qt.AlignVCenter
                    onClicked:{
                        isManuallyResizing=true
                        timer.restart()
                    }
                }

                ToolButton {
                    id: buttonWindowMinimize
                    Layout.alignment:  Qt.AlignVCenter
                    icon.name: "edit-cut"
                    icon.source: "images/cut.png"

                    onClicked:wm.minimize(windowId)
                }
                ToolButton {
                    id: buttonWindowRestore
                    text: "res"
                    Layout.alignment:  Qt.AlignVCenter
                    onClicked:{wm.restore(windowId)}
                }
                ToolButton {
                    id: buttonWindowMaximize
                    text: "max"
                    Layout.alignment:  Qt.AlignVCenter
                    onClicked:wm.maximize(windowId)
                }
                ToolButton {
                    id: buttonWindowClose
                    text: "clo"
                    Layout.alignment:  Qt.AlignVCenter
                    onClicked:wm.closeWindow(windowId)
                }
            }


        }
        Rectangle{
            id: sizeRect
            visible: false
            anchors.fill: nodeRectangle
            color: "#350312d1"
            opacity: 1
            z:10

            Rectangle {
                id: leftBall
                visible: false
                z:10
                width: 40
                height: 40
                color: "steelblue"
                radius: 40
                anchors.horizontalCenter: sizeRect.left
                anchors.verticalCenter: sizeRect.verticalCenter
                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.XAxis }
                    onMouseXChanged: {
                        if(drag.active){
                            timer.restart();
                            if (nodeRectangle.parent.width - mouse.x < 140){
                                return
                            }
                            nodeRectangle.parent.width = nodeRectangle.parent.width - mouse.x
                            nodeRectangle.parent.x = nodeRectangle.parent.x + mouse.x
                        }
                    }
                }
            }
            Rectangle {
                id: rightBall
                visible: false
                z:10
                width: 40
                height: 40
                color: "steelblue"
                radius: 40
                anchors.horizontalCenter: sizeRect.right
                anchors.verticalCenter: sizeRect.verticalCenter
                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.XAxis }
                    onMouseXChanged: {
                        if(drag.active){
                            timer.restart();
                            if (nodeRectangle.parent.width + mouse.x < 140){
                                return
                            }
                            nodeRectangle.parent.width = nodeRectangle.parent.width + mouse.x
                        }
                    }
                }
            }
            Rectangle {
                id: topBall
                visible: false
                z:10
                width: 40
                height: 40
                color: "steelblue"
                radius: 40
                anchors.horizontalCenter: sizeRect.horizontalCenter
                anchors.verticalCenter: sizeRect.top
                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.YAxis }
                    onMouseYChanged: {
                        if(drag.active){
                            //var globalMouse = nodeRectangle.parent.mapToGlobal(mouse.x, mouse.y);
                            //This is not needed any longer, will remove
                            timer.restart();
                            if (nodeRectangle.parent.height - mouse.y < title.height*2){
                                return
                            }
                            nodeRectangle.parent.height = nodeRectangle.parent.height - mouse.y
                            nodeRectangle.parent.y = nodeRectangle.parent.y + mouse.y
                        }
                    }
                }
            }
            Rectangle {
                id: bottomBall
                visible: false
                z:10
                width: 40
                height: 40
                color: "steelblue"
                radius: 40
                anchors.horizontalCenter: sizeRect.horizontalCenter
                anchors.verticalCenter: sizeRect.bottom
                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.YAxis }
                    onMouseYChanged: {
                        if(drag.active){
                            var globalMouse = nodeRectangle.parent.mapToGlobal(mouse.x, mouse.y);
                            timer.restart();
                            if (nodeRectangle.parent.height + mouse.y < title.height*2){
                                return
                            }
                            nodeRectangle.parent.height = nodeRectangle.parent.height + mouse.y
                        }
                    }
                }
                states: [

                    State {
                        name: ""
                        when:!isManuallyResizing
                        PropertyChanges { target: leftBall; visible: false}
                        PropertyChanges { target: rightBall; visible: false}
                        PropertyChanges { target: topBall; visible: false}
                        PropertyChanges { target: bottomBall; visible: false}
                        PropertyChanges { target: sizeRect; visible: false}
                        PropertyChanges { target: title; color: Material.backgroundColor}


                    },
                    State {
                        name: "RESIZE"
                        when: isManuallyResizing
                        PropertyChanges { target: leftBall; visible: true}
                        PropertyChanges { target: rightBall; visible: true}
                        PropertyChanges { target: topBall; visible: true}
                        PropertyChanges { target: bottomBall; visible: true}
                        PropertyChanges { target: sizeRect; visible: true}
                        PropertyChanges { target: title; color: Material.backgroundColor}


                    }

                ]
                transitions: [
                    Transition {
                        from: ""
                        to: "RESIZE"
                        PropertyAnimation { target: sizeRect; property: "opacity"; from: 0; to: 1; duration: 500}
                    },
                    Transition {
                        from: "RESIZE"
                        to: ""
                        PropertyAnimation { target: sizeRect; property: "opacity"; from: 1; to: 0; duration: 500}
                    }
                ]
            }
        }
        MouseArea{
            id: mouseAreaFocus
            width: nodeRectangle.width
            height: nodeRectangle.height-title.height
            anchors.bottom: nodeRectangle.bottom
            propagateComposedEvents: false
            onClicked: {console.log("testclick")}
            z:1

            Loader{
                id: load
                source: contents
                anchors.fill:parent
                //z:1
                clip: true
                onLoaded: {
                    console.log("Windowcontents loaded from movable window")
                    //item.fw = fw
                    item.forceActiveFocus()
                }
            }
        }
    }
}
