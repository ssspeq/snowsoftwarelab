import QtQuick
import QtQuick.Controls
import QtQuick.Layouts


FocusScope {
    id: root
    anchors.fill: parent
    property var currentEditor: (stackLayoutEditors.currentIndex===-1) ? null : stackLayoutEditors.itemAt(stackLayoutEditors.currentIndex)

    ToolBar {
        id: toolBarTop
        width: root.width
        position: ToolBar.Top
        anchors.top: root.top
        Flickable {
            id:toolBarTopFlickable
            anchors.fill:parent
            flickableDirection: Flickable.HorizontalFlick
            contentWidth: contentItem.childrenRect.width; contentHeight: contentItem.childrenRect.height

            RowLayout {
            id: buttonRowLayout
            ComboBox {
                id: fileSelector
                model: ListModel {
                    id: model
                }
                displayText:""
                Layout.preferredWidth: 200
                Layout.maximumWidth: 600
                Component.onCompleted: {
                    currentIndex=-1
                }
            }
            ToolButton {
                id: menuButton
                text: qsTr("⋮")
                onClicked: {
                    menu.open()
                }
            }
            ColumnLayout {
                Label{
                    id: cursorPositionLabel
                    text: (stackLayoutEditors.currentIndex===-1) ? "" : "C:"+currentEditor.codeTextColumnNumber
                    font.pixelSize: 10
                    visible: (stackLayoutEditors.currentIndex===-1) ? false : true
                }
                Label{
                    id: lineNumberLabel
                    font.pixelSize: 10
                    text: (stackLayoutEditors.currentIndex===-1) ? "" : "L:"+currentEditor.codeTextLineNumber
                    visible: (stackLayoutEditors.currentIndex===-1) ? false : true
                }
            }
            Item {
                Layout.fillWidth: true
            }
        }
    }
                Menu {
                    id: menu
                    MenuItem {
                        text: "Save"
                        action: saveFileAction
                    }
                    MenuItem {
                        text: "Close"
                        action: closeFileAction
                    }
                    MenuItem {
                        text: "Undo"
                        action: undoAction
                    }
                    MenuItem {
                        text: "Redo"
                        action: redoAction
                    }
                }
    }

    StackLayout {
        id: stackLayoutEditors
        currentIndex: fileSelector.currentIndex
        anchors.top: toolBarTop.bottom
        height: root.height-toolBarTop.height
        width: root.width
        property var files
        onCurrentIndexChanged: {
            //console.log(currentIndex)
        }
    }

        Shortcut {
            sequence: "Ctrl+1"
            onActivated: {
                if(editorCount >= 1) fileSelector.currentIndex=0
            }
        }
        Shortcut {
            sequence: "Ctrl+2"
            onActivated: {
                if(editorCount >= 2) fileSelector.currentIndex=1
            }
        }
        Shortcut {
            sequence: "Ctrl+3"
            onActivated: {
                if(editorCount >= 3) fileSelector.currentIndex=2
            }
        }
        Shortcut {
            sequence: "Ctrl+4"
            onActivated: {
                if(editorCount >= 4) fileSelector.currentIndex=3
            }
        }
        Shortcut {
            sequence: "Ctrl+5"
            onActivated: {
                if(editorCount >= 5) fileSelector.currentIndex=4
            }
        }
        Shortcut {
            sequence: "Ctrl+6"
            onActivated: {
                if(editorCount >= 6) fileSelector.currentIndex=5
            }
        }
        Shortcut {
            sequence: "Ctrl+7"
            onActivated: {
                if(editorCount >= 7) fileSelector.currentIndex=6
            }
        }
        Shortcut {
            sequence: "Ctrl+8"
            onActivated: {
                if(editorCount >= 8) fileSelector.currentIndex=7
            }
        }
        Shortcut {
            sequence: "Ctrl+9"
            onActivated: {
                if(editorCount >= 9) fileSelector.currentIndex=8
            }
        }
        Shortcut {
            sequence: "Ctrl+0"
            onActivated: {
                if(editorCount >= 10) fileSelector.currentIndex=9
            }
        }

        Action {
            id: openFileAction
            icon.name: "document-open"
            shortcut: StandardKey.Open
            enabled: (stackLayoutEditors.currentIndex===-1) ? false : true
        }

        Action {
            id: saveFileAction
            icon.name: "document-save"
            shortcut: StandardKey.Save
            enabled: (stackLayoutEditors.currentIndex===-1) ? false : !currentEditor.isSaved
            onTriggered: currentEditor.saveFile()
        }

        Action {
            id: closeFileAction
            icon.name: "document-close"
            shortcut: StandardKey.Close
            enabled: (stackLayoutEditors.currentIndex===-1) ? false : true
            onTriggered: {
                closeFile()
            }
        }

        Action {
            id: redoAction
            icon.name: "edit-redo"
            shortcut: StandardKey.Redo
            enabled: (stackLayoutEditors.currentIndex===-1) ? false : currentEditor.codeTextEdit.canRedo
            onTriggered: currentEditor.codeTextEdit.redo()
        }

        Action {
            id: undoAction
            icon.name: "edit-undo"
            shortcut: StandardKey.Undo
            enabled: (stackLayoutEditors.currentIndex===-1) ? false : currentEditor.codeTextEdit.canUndo
            onTriggered: currentEditor.codeTextEdit.undo()
        }

        function openFile(fw) {
            //TODO: Add empty item for when no file is opened
            //console.log("File is: "+file)
            var component = Qt.createComponent( "qrc:/CodeTextEdit.qml" );
            if( component.status !== Component.Ready )
            {
                if( component.status === Component.Error ){
                    console.debug("Error:"+ component.errorString() );
                return; // or maybe throw
                }

            }
            else {
                var editor = component.createObject( stackLayoutEditors, {fw: fw} )
                if (editor === null) {
                // Error Handling
                console.log("Error creating object");
                }
                else{
                    fileSelector.displayText = file
                    fileSelector.model.append({text: file})
                    fileSelector.currentIndex=0
                    currentEditor.forceActiveFocus()
                }
            }
        }


        function closeFile() {
            console.log("In close file")
            currentEditor.destroy()
            console.log("fileSelector.currentIndex")
            fileSelector.model.remove(fileSelector.currentIndex)
        }
        function saveFile(){
            console.log("In save file")
            currentEditor.saveFile()
        }
        function runFile(){

        }

    Component.onCompleted: {
    }
    Component.onDestruction: {
    }
}
