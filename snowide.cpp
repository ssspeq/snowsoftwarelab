#include "snowide.h"

SnowIDE::SnowIDE(QObject *parent) : QObject(parent)
{
    m_projects = new DataObjectModel(this);
    m_currentProject=nullptr;
    setRecentProjects(settings.value("recentProjects").value<QVariantList>());
}

void SnowIDE::makeNewProject(QUrl projectFolder, QString name)
{
    QString finalPath = QDir(projectFolder.toString()).filePath(name+".proj");
    QString mainFile = QDir(projectFolder.toString()).filePath("main.qml");
    Project project;
    LoadAndSave las;
    project.setName(name);
    project.setProjectFolder(projectFolder);
    project.addFile(name+".proj");
    if(las.makeFile(QUrl(mainFile)) == 0) {
        project.addFile(QString("main.qml"));
        saveProject(&project);
        loadProject(QUrl(finalPath));
    }
    else {
        emit error("Could not create project. Missing rights?");
    }
}

void SnowIDE::loadProject(QUrl url)
{
    LoadAndSave las;
    Project* project = (Project*)las.deSerializeProject(url);
    if(!project){
        emit error("Could not load project "+url.toString());
    }
    else{
        this->projects()->append(project);
        this->setCurrentProject(project);
        this->addRecentProject(url);
        emit projectLoaded(project);
    }
}

void SnowIDE::saveProject(Project *project)
{
    LoadAndSave las;
    QByteArray result = las.serializeProject(project);
    QString finalPath = QDir(project->projectFolder().toString()).filePath(project->name()+".proj");
    las.writeToFile(finalPath, result);
}

void SnowIDE::closeProject(Project *project)
{
    if(currentProject()==project){
        this->setCurrentProject(nullptr);
    }
    qint64 numProjects = this->projects()->count();
    //TODO: check of other stuff needs saving here
    for(int i=0; i<numProjects; i++){
        Project* tmp=(Project*)this->m_projects->get(i);
        if (tmp && tmp->name() == project->name()){
                    this->projects()->remove(i);

                    break;
                }

        }
    emit projectClosed();
}

void SnowIDE::addNewFileToProject(Project *project, QString file)
{
    //Maybe just add file in project and act on filesChanged signal to
    //create the file if it does not exist
    QString newFile = QDir(project->projectFolder().toString()).filePath(file);
    LoadAndSave las;
    if(las.makeFile(QUrl(newFile))==0){
            project->addFile(file);
            saveProject(project);
    }
    else{
        emit error("Could not create "+newFile);
    }
}

void SnowIDE::removeFileFromProject(Project *project, QUrl file, bool deleteOnDisk)
{
    QString tmpFile = QDir(project->projectFolder().toString()).filePath(file.toString());
    qDebug() << tmpFile;
    if(deleteOnDisk){
        LoadAndSave las;
        if(las.deleteFile(QUrl(tmpFile))!=0){
            emit error("Could not remove "+file.toString());
            return;
        }
    }
    //QString fileToRemove = file.toString().remove(project->projectFolder().toString()+"/");
    //qDebug() << fileToRemove;
    project->removeFile(QUrl(file));
    saveProject(project);
}

void SnowIDE::renameFile(Project *project, QUrl file, QUrl newFile)
{
    project->renameFile(file, newFile);
    saveProject(project);
    QString tmpFile = QDir(project->projectFolder().toString()).filePath(file.toString());
    QString tmpNewFile = QDir(project->projectFolder().toString()).filePath(newFile.toString());
    bool check = QFile::rename(QUrl(tmpFile).toLocalFile(), QUrl(tmpNewFile).toLocalFile());
    if(!check){
        emit error("Could not rename file");
    }
}
