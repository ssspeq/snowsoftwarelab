import QtQuick.Controls
import Qt.labs.settings
import QtQuick.Layouts
import QtQuick
import Qt.labs.platform as Platform
import SnowModule
import Qt.labs.folderlistmodel

//TODO: fix this to not rely on external stuff

ListView {
    signal openFile(string file)
    signal removeFile(string file)
    signal renameFile(string file)
    property alias folder: folderModel.folder
    property alias rootFolder: folderModel.rootFolder
    property alias nameFilters: folderModel.nameFilters
    SystemPalette { id: folderListViewPalette; colorGroup: SystemPalette.Active }
    id: folderListView
    highlight: Rectangle { color: folderListViewPalette.highlight; radius: 0 }
    focus: true
    clip: true
    spacing: 0
    model: folderModel
    delegate: fileDelegate
    FolderListModel {
        id: folderModel
        showDirs: true
        showDirsFirst: true
        showDotAndDotDot: true
        nameFilters: ["*"]
    }

    Component {
        id: fileDelegate
        Item {
            width: folderListView.width
            height: 40
            RowLayout {
                anchors.fill: parent
                Image {
                    id: folderImage
                    source: "qrc:/icons/icons-dark/places/22/folder.svg"
                    visible: folderModel.isFolder(index)? true: false
                    Layout.preferredHeight: 20
                    Layout.preferredWidth: 20
                }
                MouseArea {
                    Layout.preferredHeight: parent.height-1
                    Layout.preferredWidth: parent.width
                    Layout.fillWidth: true
                    onDoubleClicked: {
                        folderListView.currentIndex = index
                        if(folderModel.isFolder(index)){
                            folder=folderModel.get(folderListView.currentIndex, "fileUrl")
                            nameFilters=["*"]
                            console.log(folder)
                        }
                        else{
                            openFile(folderModel.get(folderListView.currentIndex, "fileUrl"))
                        }

                    }
                    onClicked: {
                        folderListView.currentIndex = index
                    }
                    onPressAndHold: {
                        folderListView.currentIndex = index
                        fileMenu.open()
                    }
                    Text {
                        text: fileName
                        color: folderModel.isFolder(index)? folderListViewPalette.dark: folderListViewPalette.text
                        elide: Text.ElideRight
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                    }
                }

                Rectangle {
                    color: folderListViewPalette.shadow
                    Layout.preferredHeight: 1
                    Layout.fillWidth: true
                    visible: (index !== (folderListView.count - 1))
                }
            }
        }
    }
    Menu {
        id: fileMenu
        MenuItem {
            id: menuItemOpenFile
            //action: openFileAction
            text: qsTr("Open")
            onClicked: {
                folderListView.openFile(folderModel.get(folderListView.currentIndex, "fileUrl"))
            }
        }
        MenuItem {
            id: menuItemRemoveFile
            //action: removeFileAction
            text: qsTr("Remove")
            onClicked: {
                folderListView.removeFile(folderModel.get(folderListView.currentIndex, "fileUrl"))
            }
        }
        MenuItem {
            id: menuItemRenameFile
            //action: renameFileAction
            text: qsTr("Rename")
            onClicked: {
                folderListView.renameFile(folderModel.get(folderListView.currentIndex, "fileUrl"))
            }
        }
    }
}
