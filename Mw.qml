import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Rectangle {
    id: root
    x: 10; y: 10
    width: 300; height: 300
    color: "transparent"
    Drag.active: dragArea.drag.active
    border.width: 1
    border.color: palette.highlight
    z: loader.activeFocus ? 1 : 0

    property int minimumWith:180
    property string file
    property int windowId
    property int oldX
    property int oldY
    property int oldWidth
    property int oldHeight
    property int lastPositionX
    property int lastPositionY
    property int lastWidth
    property int lastHeight
    property int mNewX
    property int mNewY
    property int mNewWidth
    property int mNewHeight

    property bool isMaximized: false
    property alias contents: loader.source
    property alias loadedItem: loader.item

    signal minimizeButtonPressed(int windowId)
    signal maximizeButtonPressed(int windowId)
    signal closeButtonPressed(int windowId)
    signal requestFocus(int windowId)

    SystemPalette { id: palette; colorGroup: SystemPalette.Active }

    states: [
            State {
                name: "MANUALRESIZE"
                PropertyChanges { target: topLeftBall; visible: true }
                PropertyChanges { target: bottomLeftBall; visible: true }
                PropertyChanges { target: topRightBall; visible: true }
                PropertyChanges { target: bottomRightBall; visible: true }
            },
        State {
            name: "RESIZE"
            PropertyChanges {
                      target: root;
                      explicit: true;
                      restoreEntryValues: false
                      x: mNewX;
                      y: mNewY;
                      width: mNewWidth;
                      height: mNewHeight;
                  }

        }
        ]
    transitions: [
            Transition {
                from: ""
                to: "RESIZE"
                PropertyAnimation { target: root; properties: "x, y, width, height"; easing.type: Easing.InOutQuad }
                onRunningChanged: {
                            if ((state == "RESIZE") && (!running))
                            {
                                console.log("Resetting ForcedResizing")
                                state = ""
                            }
                }
            }
    ]


    Timer {
        id: timer
            interval: 2000; running: false; repeat: false; triggeredOnStart: false
            onTriggered: {
                console.log("timeout");
                state=""
            }
        }


    Rectangle {
                id: topLeftBall
                visible: false
                z:10
                width: 40
                height: 40
                color: "steelblue"
                radius: 40
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.margins: -20 // Sets all margins at once
                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.XAndYAxis }
                    onMouseXChanged: {
                        if(drag.active){
                            if (root.width - mouse.x < minimumWith){
                                                        return
                                                    }
                            timer.restart();
                            root.width = root.width - mouse.x
                            root.x = root.x + mouse.x
                        }

                    }
                    onMouseYChanged: {
                        if(drag.active){
                            timer.restart();
                            root.height = root.height - mouse.y
                            root.y = root.y + mouse.y
                        }

                    }
                }
    }

    Rectangle {
                id: bottomLeftBall
                visible: false
                z:10
                width: 40
                height: 40
                color: "steelblue"
                radius: 40
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.margins: -20 // Sets all margins at once
                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.XAndYAxis }
                    onMouseXChanged: {
                        if(drag.active){
                            if (root.width - mouse.x < minimumWith){
                                                        return
                                                    }
                            timer.restart();
                            root.width = root.width - mouse.x
                            root.x = root.x + mouse.x
                        }

                    }
                    onMouseYChanged: {
                        if(drag.active){
                            if (root.height + mouse.y < windowToolbar.height*2){
                                                        return
                                                    }
                            timer.restart();
                            root.height = root.height + mouse.y
                        }

                    }
                }
    }

    Rectangle {
                id: topRightBall
                visible: false
                z:10
                width: 40
                height: 40
                color: "steelblue"
                radius: 40
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.margins: -20 // Sets all margins at once
                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.XAndYAxis }
                    onMouseXChanged: {
                        if(drag.active){
                            if (root.width + mouse.x < minimumWith){
                                                        return
                                                    }
                            timer.restart();
                            root.width = root.width + mouse.x
                        }

                    }
                    onMouseYChanged: {
                        if(drag.active){
                            timer.restart();
                            root.y = root.y + mouse.y
                            root.height = root.height - mouse.y
                        }

                    }
                }
    }

    Rectangle {
                id: bottomRightBall
                visible: false
                z:10
                width: 40
                height: 40
                color: "steelblue"
                radius: 40
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.margins: -20 // Sets all margins at once
                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.XAndYAxis }
                    onMouseXChanged: {
                        if(drag.active){
                            if (root.width + mouse.x < minimumWith){
                                                        return
                                                    }
                            timer.restart();
                            root.width = root.width + mouse.x
                        }

                    }
                    onMouseYChanged: {
                        if(drag.active){
                            if (root.height + mouse.y < windowToolbar.height*2){
                                                        return
                                                    }
                            timer.restart();
                            //root.y = root.y + mouse.y
                            root.height = root.height + mouse.y
                        }

                    }
                }
    }

    ColumnLayout {
        id:mainColumn
        spacing: 0
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: parent.border.width
        anchors.topMargin: parent.border.width
        anchors.rightMargin: parent.border.width
        anchors.bottomMargin: parent.border.width
        Rectangle {
            id: windowToolbar
            Layout.alignment: Qt.AlignTop
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: 35
            color: loader.activeFocus ? palette.highlight : palette.dark
            MouseArea {
                id: dragArea
                anchors.fill: parent
                drag.target: root
                onClicked: loader.forceActiveFocus()
            }
            ToolButton {
                id: resizeToolButton
                action: resizeWindowAction
                anchors.right: minimizeToolButton.left
                anchors.verticalCenter: parent.verticalCenter
            }
            ToolButton {
                id: minimizeToolButton
                action: minimizeWindowAction
                anchors.right: maximizeToolButton.left
                anchors.verticalCenter: parent.verticalCenter
            }
            ToolButton {
                id: maximizeToolButton
                action: maximizeWindowAction
                anchors.right: closeToolButton.left
                anchors.verticalCenter: parent.verticalCenter
            }
            ToolButton {
                id: closeToolButton
                action: closeWindowAction
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
            }
        }
        FocusScope {
            id: scope
            focus: true
            Layout.alignment: Qt.AlignBottom
            Layout.fillHeight: true
            Layout.preferredWidth: parent.width
            Layout.preferredHeight: parent.height-windowToolbar.height
            Loader{
                id: loader
                clip: true
                width:parent.width
                height: parent.height
                onLoaded: {
                    item.forceActiveFocus()
                }
                onStatusChanged: {
                            if( loader.status === Loader.Error ){
                                console.debug("Error:"+ loader.sourceComponent.errorString());
                            }
                            else if(loader.status === Loader.Ready){

                            }
                        }
            }
        }
    }

    Action {
        id: minimizeWindowAction
        icon.name: "window-minimize"
        shortcut: "Ctrl+M"
        onTriggered: minimizeButtonPressed(windowId)
    }
    Action {
        id: maximizeWindowAction
        icon.name: isMaximized ? "window-restore" : "window-maximize"
        shortcut: "Ctrl+L"
        onTriggered: maximizeButtonPressed(windowId)
    }
    Action {
        id: closeWindowAction
        icon.name: "window-close"
        shortcut: "Ctrl+T"
        onTriggered: closeButtonPressed(windowId)
    }
    Action {
        id: resizeWindowAction
        icon.name: "zoom-best-fit"
        //shortcut: "Ctrl+T"
        onTriggered: {
            console.log("Resize pressed")
            state="MANUALRESIZE"
        }
    }

    function animatedResizeWindow(x, y, width, height){
           mNewX = x
           mNewY = y
           mNewWidth = width
           mNewHeight = height
           lastPositionX = root.x
           lastPositionY = root.y
           lastWidth = root.width
           lastHeight = root.height
           state="RESIZE"
       }

    function animatedMaximizeWindow(newWidth, newHeight){
        oldX=x
        oldY=y
        oldWidth=width
        oldHeight=height
        mNewX = 0
        mNewY = 0
        mNewWidth = newWidth
        mNewHeight = newHeight
        state="RESIZE"
        isMaximized=true
    }

    function animatedRestoreWindow(){
        mNewX=oldX
        mNewY=oldY
        mNewWidth=oldHeight
        mNewHeight=oldWidth
        state="RESIZE"
        isMaximized=false
    }
}
