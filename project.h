#ifndef PROJECT_H
#define PROJECT_H

#include <QObject>
#include <QUrl>
#include <QStringList>
#include <QList>
#include <filewrapper.h>
#include <QtQml>

class Project : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QStringList files READ files WRITE setFiles NOTIFY filesChanged)
    Q_PROPERTY(QUrl projectFolder READ projectFolder WRITE setProjectFolder NOTIFY projectFolderChanged)
    Q_PROPERTY(QList<FileWrapper*> openFiles READ openFiles WRITE setOpenfiles NOTIFY openFilesChanged)
    QML_ELEMENT

    QString m_name;

    QStringList m_files;

    QUrl m_projectFolder;

    QList<FileWrapper*> m_openFiles;

public:
    explicit Project(QObject *parent = nullptr);

    QString name() const
    {
        return m_name;
    }

    QStringList files() const
    {
        return m_files;
    }

    QUrl projectFolder() const
    {
        return m_projectFolder;
    }

    QList<FileWrapper*> openFiles() const
    {
        return m_openFiles;
    }

public slots:

    void setName(QString name)
    {
        if (m_name == name)
            return;

        m_name = name;
        emit nameChanged(m_name);
    }

    void setFiles(QStringList files)
    {
        if (m_files == files)
            return;

        m_files = files;
        emit filesChanged(m_files);
    }

    void addFile(QUrl file)
    {
        m_files.append(file.toString());
        emit filesChanged(m_files);
    }

    void removeFile(QUrl file)
    {
        for(int i=0; i<m_files.count(); i++){
            int x = QString::compare(file.toString(), m_files[i]);
            qDebug() << m_files[i];
            if(x==0){
                m_files.removeAt(i);
                emit filesChanged(m_files);
                qDebug() << "Found file in files";
                break;
            }
        }
    }

    void renameFile(QUrl file, QUrl newFile)
    {
        qDebug() << file.toString() << newFile.toString();
        for(int i=0; i<m_files.count(); i++){
            int x = QString::compare(file.toString(), m_files[i]);
            qDebug() << m_files[i];
            if(x==0){
                m_files.removeAt(i);
                m_files.append(newFile.toString());
                qDebug() << m_files[i];
                emit filesChanged(m_files);
                qDebug() << "Found file in files";
                break;
            }
        }
    }

    void setProjectFolder(QUrl projectFolder)
    {
        if (m_projectFolder == projectFolder)
            return;

        m_projectFolder = projectFolder;
        emit projectFolderChanged(m_projectFolder);
    }

    void setOpenfiles(QList<FileWrapper*> openFiles)
    {
        if (m_openFiles == openFiles)
            return;

        m_openFiles = openFiles;
        emit openFilesChanged(m_openFiles);
    }

    FileWrapper* getFileWrapper(QUrl file)
    {
        //Check if we got relative url
        if(file.isRelative()){
            QString mFile = QDir(this->projectFolder().toString()).filePath(file.toString());
            qDebug() << mFile;
            file=mFile;
        }
        FileWrapper* tmp;
        for(int i=0; i<m_openFiles.count(); i++){
            tmp = (FileWrapper*)m_openFiles[i];
            if(tmp->url()==file){
                return tmp;
            }
        }
        FileWrapper* fw = new FileWrapper(this);
        fw->setUrl(file);
        m_openFiles.append(fw);
        return fw;
    }

signals:

    void nameChanged(QString name);
    void filesChanged(QStringList files);
    void projectFolderChanged(QUrl projectFolder);
    void error(QString error);
    void openFilesChanged(QList<FileWrapper*> openFiles);
};

#endif // PROJECT_H
