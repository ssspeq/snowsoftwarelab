#pragma once

#include <QQmlApplicationEngine>
#include <QtQml>

class EnhancedQmlApplicationEngine : public QQmlApplicationEngine
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit EnhancedQmlApplicationEngine(QObject *parent = nullptr);

    Q_INVOKABLE void clearCache();
};
