cmake_minimum_required(VERSION 3.14)

project(SnowSoftwareLab LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# QtCreator supports the following variables for Android, which are identical to qmake Android variables.
# Check https://doc.qt.io/qt/deployment-android.html for more information.
# They need to be set before the find_package(...) calls below.

if(ANDROID)
    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
    if (ANDROID_ABI STREQUAL "armeabi-v7a")
        set(ANDROID_EXTRA_LIBS
            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libcrypto.so
            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libssl.so)
    endif()
endif()


find_package(QT NAMES Qt6 Qt5 COMPONENTS Core Quick Qml Widgets REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Core Quick Qml Widgets REQUIRED)

#find_package(Qt6 COMPONENTS QuickControls2 REQUIRED)
#target_link_libraries(SnowSoftwareLab PRIVATE Qt6::QuickControls2)
#target_link_libraries(SnowSoftwareLab PRIVATE Core)

set(PROJECT_SOURCES
        main.cpp
        qml.qrc
        variable.cpp
        windowmanager.cpp
        project.cpp
        qtserialize.cpp
        qtdeserialize.cpp
        dataobjectmodel.cpp
        qqmlobjectlistmodel.cpp
        loadandsave.cpp
        fileoperations.cpp
        FileOps.cpp
        qttypesobject.cpp
        filewrapper.cpp
        snowide.cpp
        codeeditor.cpp
        qsourcehighliter.cpp
        qsourcehighliterthemes.cpp
        languagedata.cpp
        enhancedqmlapplicationengine.cpp
)

if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
    qt_add_executable(SnowSoftwareLab
        ${PROJECT_SOURCES}
    )
else()
    if(ANDROID)
        add_library(SnowSoftwareLab SHARED
            ${PROJECT_SOURCES}
        )
    else()
        add_executable(SnowSoftwareLab
          ${PROJECT_SOURCES}
        )
    endif()
endif()

target_compile_definitions(SnowSoftwareLab
  PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(SnowSoftwareLab
  PRIVATE Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Quick)
target_link_libraries(SnowSoftwareLab PRIVATE Qt6::Widgets)
find_package(Qt6 COMPONENTS Qml Core5Compat REQUIRED)
target_link_libraries(SnowSoftwareLab PRIVATE Qt6::Qml Qt6::Core5Compat)

set_target_properties(SnowSoftwareLab
    PROPERTIES QT_QML_MODULE_VERSION 1.0
    QT_QML_MODULE_URI SnowModule
)
qt6_qml_type_registration(SnowSoftwareLab)

