import QtQuick.Controls
import Qt.labs.settings
import QtQuick.Layouts
import QtQuick
import Qt.labs.platform as Platform
import SnowModule
import Qt.labs.folderlistmodel

Rectangle {
    id: recentProjectsView
    border.color: myPalette.highlight
    border.width: 2
    visible: {if(ide.currentProject){
            return false
        }
        else{
            return true
        }
    }
    Rectangle {
        id: main
        width: recentProjectsView.width-recentProjectsView.border.width;
        height: recentProjectsView.height-recentProjectsView.border.width;
        anchors.centerIn: parent
        color: myPalette.light
        clip: true
        ColumnLayout {
            spacing: 0
            Label {
                id: recentProjectsLabel
                //Layout.alignment: Qt.AlignTop
                text: "Recent projects"
                Layout.preferredWidth: main.width
                Layout.preferredHeight: 20
                background: Rectangle {
                    color: myPalette.base
                }
            }
            ListView {
                id: recentProjectsListView
                Layout.alignment: Qt.AlignBottom
                Layout.fillHeight: true
                Layout.preferredWidth: main.width
                Layout.preferredHeight: main.height-recentProjectsLabel.height
                highlight: Rectangle { color: myPalette.highlight; radius: 0 }
                focus: true
                clip: true
                spacing: 0
                model: ide.recentProjects
                delegate: projectDelegate

                Component {
                    id: projectDelegate

                    ColumnLayout {
                        width: parent.width

                        Text {
                            text: modelData
                            color: myPalette.text
                            elide: Text.ElideRight
                            Layout.fillWidth: true
                            verticalAlignment: Text.AlignLeft
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    recentProjectsListView.currentIndex = index
                                    ide.loadProject(modelData)
                                }
                            }
                        }


                        Rectangle {
                            color: myPalette.shadow
                            Layout.preferredHeight: 1
                            Layout.fillWidth: true
                            visible: (index !== (recentProjectsListView.count - 1))
                        }
                    }
                }
            }
        }
    }
}
