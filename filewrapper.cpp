#include "filewrapper.h"

FileWrapper::FileWrapper(QObject *parent) : QObject(parent)
{
    m_file = new QFile();
    m_fileWatcher = new QFileSystemWatcher();
    this->setIsSaved(false);
    QObject::connect(this->fileWatcher(), SIGNAL(fileChanged(QString)), this, SLOT(notifyFileChanged(QString)));
}

void FileWrapper::notifyFileChanged(QString path)
{
    qDebug() << "File changed" << path;
    emit fileChangedOnDisk(path);
    if (QFile::exists(path)) {
        this->fileWatcher()->addPath(path);
    }

}

bool FileWrapper::saveText(QString contents)
{
    QFile file;
    if(this->url().isLocalFile()){
        file.setFileName(this->url().toLocalFile());
    }
    else{
        file.setFileName(this->url().toString());
    }

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)){
        return false;
    }
    QTextStream out(&file);
    out << contents;

    file.close();
    this->setIsSaved(true);
    return true;
}

QString FileWrapper::readText()
{
    QFile file;
    if(this->url().isLocalFile()){
        file.setFileName(this->url().toLocalFile());
    }
    else{
        file.setFileName(this->url().toString());
    }
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        //Raise error here
        qDebug() << "file error: " << this->file()->errorString();
    return nullptr;
    }
    QTextStream in(&file);
    while (!in.atEnd()) {
        QString line = in.readAll();
        file.close();
        this->setIsSaved(true);
        return line;
    }
    return nullptr;
}
