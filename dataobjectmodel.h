#pragma once

#include <QObject>
#include <QAbstractListModel>
#include <QQmlListProperty>
#include <QtQml>

class DataObjectModel : public QAbstractListModel {
    Q_OBJECT
    Q_DISABLE_COPY(DataObjectModel)

    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(QQmlListProperty<QObject> content READ content)
    //Q_PROPERTY(QList<QObject*> data READ data WRITE setdata NOTIFY dataChanged)
    Q_CLASSINFO("DefaultProperty", "content")
    QML_ELEMENT

public:
    explicit DataObjectModel(QObject *parent = nullptr);
    static void registerTypes(const char *uri);
    QQmlListProperty<QObject> content();

    int rowCount(const QModelIndex &p) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    long long int count() const;

public slots:
    Q_INVOKABLE void append(QObject* o);
    Q_INVOKABLE void insert(QObject* o, int i);
    Q_INVOKABLE QObject* get(int i);
    Q_INVOKABLE void remove(int i);
    Q_INVOKABLE QObject *getByProperty(QString property, QVariant value);

    static void dataObjectAppend(QQmlListProperty<QObject> *list, QObject *e);
    static long long int dataObjectCount(QQmlListProperty<QObject> *list);
    static QObject* dataObjectAt(QQmlListProperty<QObject> *list, long long i);
    static void dataObjectClear(QQmlListProperty<QObject> *list);

//    void setdata(QList<QObject*> data)
//    {
//        if (m_data == data)
//            return;

//        m_data = data;
//        emit dataChanged(m_data);
//    }

signals:
    void countChanged(int count);

    void dataChanged(QList<QObject*> data);

private:
    QList<QObject*> m_data;
};
