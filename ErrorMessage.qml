import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Rectangle {
    id: root
    color: "transparent"
    opacity: 0.0
    property alias enabled: closeButton.enabled
    property int dialogWidth: 300
    property int dialogHeight: 200
    state: enabled ? "on" : "baseState"

    states: [
        State {
            name: "on"
            PropertyChanges {
                target: root
                opacity: 1.0
            }
        }
    ]

    transitions: [
        Transition {
            from: "*"
            to: "*"
            NumberAnimation {
                properties: "opacity"
                easing.type: Easing.OutQuart
                duration: 1000
            }
        }
    ]

    Rectangle {
        anchors.centerIn: parent
        width: dialogWidth
        height: dialogHeight
        radius: 5
        color: "red"

        ColumnLayout {
            anchors.centerIn: parent
            clip: true
        Text {
            id: text
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: dialogWidth
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: "white"
            wrapMode: Text.WordWrap
        }
        Button {
            id: closeButton
            Layout.alignment: Qt.AlignCenter
            text: "DAMNIT!"
            onClicked: root.enabled = false
            }
        }
    }

    function show(msg) {
        text.text = "<b>ERROR</b><br><br>"
        text.text = text.text + msg
        root.enabled = true
    }
}
