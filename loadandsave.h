#ifndef LOADANDSAVE_H
#define LOADANDSAVE_H

#include <QObject>
#include <qqml.h>
#include "qtserialize.h"
#include "qtdeserialize.h"
#include <QFile>
#include <QSaveFile>
#include <project.h>
#include <filewrapper.h>

class LoadAndSave : public QObject
{
    Q_OBJECT
    QML_ELEMENT

public:
    explicit LoadAndSave(QObject *parent = nullptr);

public slots:
    QByteArray serializeProject(QObject *obj);
    QObject *deSerializeProject(QUrl url);
    int writeToFile(QUrl url, QByteArray ba);
    QByteArray readFromFile(QUrl url);
    int makeFile(QUrl file);

    int deleteFile(QUrl file);
signals:

};

#endif // LOADANDSAVE_H
