include( ../common.pri )
include( ../coverage.pri )

QT -= gui

TARGET = qtserialize
TEMPLATE = lib

DEFINES += QTSERIALIZE_LIBRARY

SOURCES += qtserialize.cpp \
    qtdeserialize.cpp

HEADERS += qtserialize.h\
    qtserialize_global.h \
    qtdeserialize.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
