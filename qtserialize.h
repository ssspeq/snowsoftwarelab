/****************************************************************************
** This is free and unencumbered software released into the public domain.
**
** Anyone is free to copy, modify, publish, use, compile, sell, or
** distribute this software, either in source code form or as a compiled
** binary, for any purpose, commercial or non-commercial, and by any
** means.
**
** In jurisdictions that recognize copyright laws, the author or authors
** of this software dedicate any and all copyright interest in the
** software to the public domain. We make this dedication for the benefit
** of the public at large and to the detriment of our heirs and
** successors. We intend this dedication to be an overt act of
** relinquishment in perpetuity of all present and future rights to this
** software under copyright law.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
** OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
** ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
** OTHER DEALINGS IN THE SOFTWARE.
**
** For more information, please refer to <http://unlicense.org>
****************************************************************************/

#ifndef QTSERIALIZE_H
#define QTSERIALIZE_H

#include "qtserialize_global.h"

class QByteArray;
class QObject;

namespace vs
{
/**
 * @brief The QtSerialize class serializes a QObject with it's properies to JSON
 */
class QTSERIALIZESHARED_EXPORT QtSerialize
{
public:
    QtSerialize();
    ~QtSerialize();

    /**
     * Serializes a QObject to a string
     */
    QByteArray serialize(QObject* obj);

    /**
     * Clears the current blacklist. Blacklistet property(names) are not serialized.
     * The property "objectName" is blacklisted by default.
     * @see addBlackListItem
     */
    void clearBlackList();
    /**
     * Adds a property to the list of properties that are not serialized
     * @param name name of the property not to serialize
     * @see clearBlackList
     */
    void addBlackListItem(const QString& name);

private:
    class QtSerializePrivate;
    Q_DISABLE_COPY(QtSerialize)
    Q_DECLARE_PRIVATE(QtSerialize)
    QtSerializePrivate* const d_ptr;
};
}

#endif // QTSERIALIZE_H
