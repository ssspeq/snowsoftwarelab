import QtQuick.Controls
import Qt.labs.settings
import QtQuick.Layouts
import QtQuick
import Qt.labs.platform as Platform
import SnowModule

ListView {
    signal openFile(string file)
    signal removeFile(string file)
    signal renameFile(string file)
    property alias model: projectFilesView.model
    SystemPalette { id: palette; colorGroup: SystemPalette.Active }
    id: projectFilesView
    highlight: Rectangle { color: palette.highlight; radius: 0 }
    focus: true
    clip: true
    spacing: 0
    delegate: fileDelegate

    Component {
        id: fileDelegate
        Item {
            width: projectFilesView.width
            height: 40
            RowLayout {
                anchors.fill: parent
                MouseArea {
                    Layout.preferredHeight: parent.height-1
                    Layout.preferredWidth: parent.width
                    Layout.fillWidth: true
                    onDoubleClicked: {
                        projectFilesView.currentIndex = index
                        openFile(modelData)
                    }
                    onClicked: {
                        projectFilesView.currentIndex = index
                    }
                    onPressAndHold: {
                        projectFilesView.currentIndex = index
                        fileMenu.open()
                    }
                    Text {
                        text: modelData
                        elide: Text.ElideRight
                        verticalAlignment: Text.AlignVCenter
                        anchors.fill: parent
                    }
                }

                Rectangle {
                    color: palette.shadow
                    Layout.preferredHeight: 1
                    Layout.fillWidth: true
                    visible: (index !== (projectFilesView.count - 1))
                }
            }
        }
    }
    Menu {
        id: fileMenu
        MenuItem {
            id: menuItemOpenFile
            //action: openFileAction
            text: qsTr("Open")
            onClicked: {
                openFile(model[projectFilesView.currentIndex])
            }
        }
        MenuItem {
            id: menuItemRemoveFile
            //action: removeFileAction
            text: qsTr("Remove")
            onClicked: {
                removeFile(model[projectFilesView.currentIndex])
            }
        }
        MenuItem {
            id: menuItemRenameFile
            //action: renameFileAction
            text: qsTr("Rename")
            onClicked: {
                renameFile(model[projectFilesView.currentIndex])
            }
        }
    }
}
