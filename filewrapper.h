#ifndef FILEWRAPPER_H
#define FILEWRAPPER_H

#include <QObject>
#include <QDebug>
#include <QUrl>
#include <QFile>
#include <QString>
#include <QStringList>
#include <QDebug>
#include <QTextStream>
#include <QFileSystemWatcher>
#include <QDir>
#include <QList>
#include <QtQml>

class FileWrapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QFile* file READ file WRITE setFile NOTIFY fileChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(QString version READ version WRITE setVersion NOTIFY versionChanged)
    Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QString contents READ contents WRITE setContents NOTIFY contentsChanged)
    Q_PROPERTY(QString contentsFromDisk READ contentsFromDisk WRITE setContentsFromDisk NOTIFY contentsFromDiskChanged)
    Q_PROPERTY(bool isSaved READ isSaved WRITE setIsSaved NOTIFY isSavedChanged)
    Q_PROPERTY(QFileSystemWatcher* fileWatcher READ fileWatcher WRITE setFileWatcher NOTIFY fileWatcherChanged)
    Q_PROPERTY(QList<QObject*> views READ views WRITE setViews NOTIFY viewsChanged)
    QML_ELEMENT

QString m_name;

QString m_description;

QString m_version;

bool m_isSaved = false;

QString m_type;

QUrl m_url;

QFile* m_file = nullptr;

QString m_contents;

QFileSystemWatcher* m_fileWatcher = nullptr;

QString m_contentsFromDisk;

QList<QObject*> m_views;

public:
    Q_INVOKABLE explicit FileWrapper(QObject *parent = nullptr);

QString contentsFromDisk() const
{
    return m_contentsFromDisk;
}

QList<QObject*> views() const
{
    return m_views;
}

public slots:
    void notifyFileChanged(QString path);

QString name() const
{
    return m_name;
}

QString description() const
{
    return m_description;
}

QString version() const
{
    return m_version;
}

bool isSaved() const
{
    return m_isSaved;
}

QString type() const
{
    return m_type;
}

QUrl url() const
{
    return m_url;
}

QFile* file() const
{
    return m_file;
}

QString contents() const
{
    return m_contents;
}

QFileSystemWatcher* fileWatcher() const
{
    return m_fileWatcher;
}

public slots:

bool saveText(QString contents);

QString readText();

void setName(QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(m_name);
}

void setDescription(QString description)
{
    if (m_description == description)
        return;

    m_description = description;
    emit descriptionChanged(m_description);
}

void setVersion(QString version)
{
    if (m_version == version)
        return;

    m_version = version;
    emit versionChanged(m_version);
}

void setIsSaved(bool isSaved)
{
    if (m_isSaved == isSaved)
        return;

    m_isSaved = isSaved;
    emit isSavedChanged(m_isSaved);
}

void setType(QString type)
{
    if (m_type == type)
        return;

    m_type = type;
    emit typeChanged(m_type);
}

void setUrl(QUrl url)
{
    if (m_url == url)
        return;

    m_url = url;
    if(!url.isLocalFile()){
        this->file()->setFileName(QDir::toNativeSeparators(m_url.toLocalFile()));
        this->fileWatcher()->addPath(m_url.path());
    }
    //If not local file
    else if(m_url.toString().contains("content:/")){
        qDebug() << "Url har android beginning";
        QString tmp = m_url.toString();
        tmp.replace("content:/", "file:/");
        m_url.setUrl(tmp);
        this->file()->setFileName(m_url.toString());
        this->fileWatcher()->addPath(m_url.path());
    }
    else if(url.isLocalFile()){
        this->file()->setFileName(m_url.toString());
        this->fileWatcher()->addPath(m_url.path());
    }
    emit urlChanged(m_url);
}

void setFile(QFile* file)
{
    if (m_file == file)
        return;

    m_file = file;
    emit fileChanged(m_file);
}

void setContents(QString contents)
{
    if (m_contents == contents)
        return;

    m_contents = contents;
    emit contentsChanged(m_contents);
}

void setFileWatcher(QFileSystemWatcher* fileWatcher)
{
    if (m_fileWatcher == fileWatcher)
        return;

    m_fileWatcher = fileWatcher;
    emit fileWatcherChanged(m_fileWatcher);
}

void setContentsFromDisk(QString contentsFromDisk)
{
    if (m_contentsFromDisk == contentsFromDisk)
        return;

    m_contentsFromDisk = contentsFromDisk;
    emit contentsFromDiskChanged(m_contentsFromDisk);
}

void setViews(QList<QObject*> views)
{
    if (m_views == views)
        return;

    m_views = views;
    emit viewsChanged(m_views);
}

void addView(QObject* view)
{
    m_views.append(view);
    emit viewsChanged(m_views);
}

signals:

void nameChanged(QString name);
void descriptionChanged(QString description);
void versionChanged(QString version);
void isSavedChanged(bool isSaved);
void typeChanged(QString type);
void urlChanged(QUrl url);
void fileChanged(QFile* file);
void fileChangedOnDisk(QString path);
void contentsChanged(QString contents);
void fileWatcherChanged(QFileSystemWatcher* fileWatcher);
void watchPathsChanged(QStringList watchPaths);

void contentsFromDiskChanged(QString contentsFromDisk);
void viewsChanged(QList<QObject*> views);
};

#endif // FILEWRAPPER_H
