#ifndef VARIABLE_H
#define VARIABLE_H

#include <QObject>
#include <QDebug>
#include <QVariant>
#include <QUuid>


class Variable : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(QString id READ id WRITE setId NOTIFY idChanged)

    QVariant m_value;

    QString m_id;

public:
    explicit Variable(QString id = "", QVariant value = "", QObject *parent = nullptr);

    QVariant value() const
    {
        return m_value;
    }

    QString id() const
    {
        return m_id;
    }

public slots:
    void setValue(QVariant value)
    {
        if (m_value == value)
            return;

        m_value = value;
        emit valueChanged(m_value);
        qDebug() << "value set";
    }

    void setId(QString id)
    {
        if (m_id == id)
            return;

        m_id = id;
        emit idChanged(m_id);
    }

signals:

void valueChanged(QVariant value);
void idChanged(QString id);
};

#endif // VARIABLE_H
