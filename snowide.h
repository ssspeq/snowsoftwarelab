#ifndef SNOWIDE_H
#define SNOWIDE_H

#include <QObject>
#include <qqml.h>
#include <QList>
#include <project.h>
#include <dataobjectmodel.h>
#include <loadandsave.h>

class SnowIDE : public QObject
{
    Q_OBJECT
    Q_PROPERTY(DataObjectModel* projects READ projects WRITE setProjects NOTIFY projectsChanged)
    Q_PROPERTY(Project* currentProject READ currentProject WRITE setCurrentProject NOTIFY currentProjectChanged)
    Q_PROPERTY(QVariantList recentProjects READ recentProjects WRITE setRecentProjects NOTIFY recentProjectsChanged)
    QML_ELEMENT

    DataObjectModel* m_projects;

    Project* m_currentProject;

    QSettings settings;

    QVariantList m_recentProjects;

public:
    explicit SnowIDE(QObject *parent = nullptr);

    DataObjectModel* projects() const
    {
        return m_projects;
    }

    Project* currentProject() const
    {
        return m_currentProject;
    }

    QVariantList recentProjects() const
    {
        return m_recentProjects;
    }

public slots:
    void setProjects(DataObjectModel* projects)
    {
        if (m_projects == projects)
            return;

        m_projects = projects;
        emit projectsChanged(m_projects);
    }

    void addProject(Project* project)
    {
        m_projects->append(project);
        emit projectsChanged(m_projects);
    }

    void removeProject(Project* project)
    {

        emit projectsChanged(m_projects);
    }

    void setCurrentProject(Project* currentProject)
    {
        if (m_currentProject == currentProject)
            return;

        m_currentProject = currentProject;
        emit currentProjectChanged(m_currentProject);
    }
    void setCurrentProjectByIndex(int index)
    {
        //TODO: Maybe check for out of bounds error here
        m_currentProject = (Project*)projects()->get(index);
        emit currentProjectChanged(m_currentProject);
    }
    void setCurrentProjectToFirstOpenProject()
    {
        if(m_projects->count()>0){
            m_currentProject = (Project*)projects()->get(0);
        }
        emit currentProjectChanged(m_currentProject);
    }

    void makeNewProject(QUrl projectFolder, QString name);
    void loadProject(QUrl url);
    void saveProject(Project* project);
    void closeProject(Project* project);
    void setRecentProjects(QVariantList recentProjects)
    {
        if (m_recentProjects == recentProjects)
            return;

        m_recentProjects = recentProjects;
        emit recentProjectsChanged(m_recentProjects);
    }

    void addRecentProject(QUrl project)
    {
        QVariant tmp;
        for(int i=0; i<m_recentProjects.count(); i++){
            tmp = m_recentProjects[i];
            if (tmp==project){
                    m_recentProjects.remove(i);
                }
            }
        m_recentProjects.prepend(project);
        //Keep only the last 10 projects
        if(m_recentProjects.count()>9){
            m_recentProjects.removeLast();
        }
        settings.setValue("recentProjects", QVariant::fromValue(m_recentProjects));
        emit recentProjectsChanged(m_recentProjects);
    }

    void addNewFileToProject(Project *project, QString file);
    void renameFile(Project *project, QUrl file, QUrl newFile);
    void removeFileFromProject(Project *project, QUrl file, bool deleteOnDisk);
signals:

    void projectsChanged(DataObjectModel* projects);
    void currentProjectChanged(Project* currentProject);
    void recentProjectsChanged(QVariantList recentProjects);
    void projectLoaded(Project* currentProject);
    void projectClosed();
    void error(QString error);
};

#endif // SNOWIDE_H
