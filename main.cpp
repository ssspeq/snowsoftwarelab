#include <QApplication>
#include <QQmlApplicationEngine>
#include <enhancedqmlapplicationengine.h>
#include <QSettings>
#include <QIcon>
int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("SnowSoftware");
    QCoreApplication::setOrganizationDomain("SnowSoftware");
    QCoreApplication::setApplicationName("Snow Software Lab");
    app.setWindowIcon(QIcon(":/icons/snowflake.svg"));
    EnhancedQmlApplicationEngine engine;
    QIcon::setThemeName( "Breeze" );
    engine.rootContext()->setContextProperty("$QmlEngine", &engine);
    //qDebug() << QIcon::themeSearchPaths();

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
