import QtQuick
import "ListProperty.js" as Lp

Item {
    id: root
    property int windowId
    property var dropArea: ""
    property var windows: []

    function editFile(fw){
        var comp = Qt.createComponent("qrc:/Mw.qml")
        function finishCreation() {
            if (comp.status === Component.Ready) {
                var newWindow = comp.createObject(dropArea, {windowId:generateNewWindowId(), x: 10, y: 10, height: 300, width: 300, contents:"qrc:/CodeEditor.qml"})
                if (newWindow === null) {
                    // Error Handling
                    console.log("Error creating object");
                }
                else {
                    newWindow.closeButtonPressed.connect(closeWindow)
                    newWindow.maximizeButtonPressed.connect(maximize)
                    newWindow.requestFocus.connect(onRequestFocus)
                    windows.push(newWindow)
                    fw.addView(newWindow)
                    newWindow.loadedItem.openFile(fw)
                }
            }
            else if (comp.status === Component.Error) {
                // Error Handling
                console.log("Error loading component:", comp.errorString());
            }
        }
        if (comp.status === Component.Ready)
            finishCreation();
        else
            comp.statusChanged.connect(finishCreation);
    }

    function runProject(file){
        var comp = Qt.createComponent("qrc:/Mw.qml")
        function finishCreation() {
            if (comp.status === Component.Ready) {
                var newWindow = comp.createObject(dropArea, {windowId:generateNewWindowId(), x: 10, y: 10, height: 300, width: 300, contents:"qrc:/UserAppLoader.qml"})
                newWindow.closeButtonPressed.connect(closeWindow)
                newWindow.maximizeButtonPressed.connect(maximize)
                newWindow.requestFocus.connect(onRequestFocus)
                windows.push(newWindow)
                newWindow.loadedItem.fw.url=file
                if (newWindow === null) {
                    // Error Handling
                    console.log("Error creating object");
                }
            }
            else if (comp.status === Component.Error) {
                // Error Handling
                console.log("Error loading component:", comp.errorString());
            }
        }
        if (comp.status === Component.Ready)
            finishCreation();
        else
            comp.statusChanged.connect(finishCreation);
    }

    function generateNewWindowId(){
        var newId=0

        for (var i = 0; i < windows.length; i++) {
            if(windows[i].windowId === newId){
                newId++
            }
            else{
                return newId
            }
        }
        return newId
    }

    function onRequestFocus(windowId){
        console.log("focus requested from window "+windowId)
    }

    function maximize(windowId){
        for (var i = 0; i < windows.length; i++)  {
            if(windows[i].windowId === windowId){
                if(!windows[i].isMaximized){
                windows[i].animatedMaximizeWindow(dropArea.width, dropArea.height)
                }
                else{
                    windows[i].animatedRestoreWindow()
                }
            }
        }
    }

    function resizeWindow(windowId, newX, newY, newWidth, newHeight){
        for (var i = 0; i < windows.length; i++)  {
            if(windows[i].windowId === windowId){
                windows[i].animatedResizeWindow(newX, newY, newWidth, newHeight)
            }
        }
    }

    function resizeWindows(rows, columns){
        var newWindowWidth=dropArea.width/columns
        var newWindowHeight=dropArea.height/rows
        var totalWindowsInLayout=rows*columns
        var currentX=0
        var currentY=0
        var currentRow=1
        var currentColumn=1
        for (var i = 0; i < windows.length; i++){
            if(windows[i]&&i<totalWindowsInLayout){
                resizeWindow(i, currentX, currentY, newWindowWidth, newWindowHeight)
                if(currentColumn<columns){
                    currentColumn++
                    currentX=currentX+newWindowWidth
                }
                else if(currentColumn===columns){
                    currentColumn=1
                    currentX=0
                    currentRow++
                    currentY=currentY+newWindowHeight
                }
            }
        }
    }


    function closeWindow(windowId){
        for (var i = 0; i < windows.length; i++)  {
            if(windows[i].windowId === windowId){
                windows[i].destroy()
                windows[i]=null
                windows.splice(i,1);
                break
            }
        }
    }
}

